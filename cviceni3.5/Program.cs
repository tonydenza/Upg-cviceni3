﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni3._5
{
    class Program
    {
        struct bod
        {
            public double x;
            public double y;
        }

        struct trojuhelnik
        {
            public bod a;
            public bod b;
            public bod c;
        }

        static void Vypis (bool tvrz)
        {
            Console.Write("{0} ", tvrz);
            Console.ReadLine();
        }

        static void Vypis(double tvrz)
        {
            Console.Write("{0} ", tvrz);
            Console.ReadLine();
        }

        static bool PribliznaRovnost (double a, double b)
        {
            double e = 10e-13;
            bool vysledek;

            if ( a - b > e || b - a > e)
            {
                vysledek = false;
            }
            else
            {
                vysledek = true;
            }

            return vysledek;
        }

        static double DelkaUsecky (bod b1, bod b2)
        {
            double vysledek = Math.Sqrt( (b2.x - b1.x) * (b2.x - b1.x) + (b2.y - b1.y) * (b2.y - b1.y) );
            return vysledek;
        }

        static bool JeTrojuhelnik (bod a, bod b, bod c)
        {
            bool tvori;
            double ab = DelkaUsecky(a, b), bc = DelkaUsecky(b, c), ca = DelkaUsecky(c, a);

            if ( ab + bc < ca || PribliznaRovnost(ab + bc, ca) )
            {
                tvori = false;
            }
            else if ( ab + ca < bc || PribliznaRovnost(ab + ca, bc) )
            {
                tvori = false;
            }
            else if ( bc + ca < ab || PribliznaRovnost( bc + ca, ab ) )
            {
                tvori = false;
            }
            else
            {
                tvori = true;
            }

            return tvori;
        }

        static double ObvodTrojuhelniku (trojuhelnik t)
        {
            double obvod = DelkaUsecky(t.a, t.b) + DelkaUsecky(t.b, t.c) + DelkaUsecky(t.a, t.c);
            return obvod;
        }

        static bool TrojuhelnikJePravouhly (trojuhelnik t)
        {
            double ab = DelkaUsecky(t.a, t.b), bc = DelkaUsecky(t.b, t.c), ca = DelkaUsecky(t.c, t.a);

            if (PribliznaRovnost(ab * ab + bc * bc, ca * ca))
                return true;
            else if (PribliznaRovnost(ab * ab + ca * ca, bc * bc))
                return true;
            else if (PribliznaRovnost(bc * bc + ca * ca, ab * ab))
                return true;
            else
                return false;
        }

        static void Main(string[] args)
        {
            bool je1, je2, je3, je4;
            bod b1, b2, b3, b4, b5, b6, b7;
            trojuhelnik t1, t2;
            double delka, obvod1, obvod2;

            b1.x = 7.2; b1.y = -2;
            b2.x = -2.9; b2.y = -6.23;
            b3.x = 2; b3.y = 5;
            b4.x = -1; b4.y = -2.5;
            b5.x = 3; b5.y = 7.5;
            b6.x = 3; b6.y = -2.6;
            b7.x = 5.2; b7.y = -2.6;
            t1.a = b1; t1.b = b2; t1.c = b3;
            t2.a = b5; t2.b = b6; t2.c = b7;

            je1 = PribliznaRovnost(5, 4.99999999999992);
            Vypis(je1);

            delka = DelkaUsecky(b1, b2);
            Vypis(delka);


            je2 = JeTrojuhelnik(b1, b2, b3);
            Vypis(je2);

            je3 = JeTrojuhelnik(b4, b3, b5);
            Vypis(je3);

            obvod1 = ObvodTrojuhelniku(t1);
            Vypis(obvod1);

            obvod2 = ObvodTrojuhelniku(t2); // obvod je 22,6368273662667 (viz. https://www.hackmath.net/cz/kalkulacka/vypocet-trojuhelnika?what=vc&a=3&a1=7%2C5&3dd=3D&a2=&b=3&b1=-2%2C6&b2=&c=5%2C2&c1=-2%2C6&c2=&submit=Vy%C5%99e%C5%A1&3d=0 )
            Vypis(obvod2);                  // v materialech je 22,967

            je4 = TrojuhelnikJePravouhly(t2);
            Vypis(je4);
        }
    }
}
