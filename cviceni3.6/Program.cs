﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni3._6
{
    class Program
    {
        enum Vyber
        {
            kamen = 1, 
            nuzky = 2, 
            papir = 3
        }

        static Vyber Dosad (string vyber)
        {
            if (vyber == "kamen" || vyber == "Kamen" || vyber == "KAMEN")
            {
                return Vyber.kamen;
            }
            else if (vyber == "nuzky" || vyber == "Nuzky" || vyber == "NUZKY")
            {
                return Vyber.nuzky;
            }
            else if (vyber == "papir" || vyber == "Papir" || vyber == "PAPIR")
            {
                return Vyber.papir;
            }
            else
            {
                return 0;
            }
        }

        static Vyber Dosad(int vyber)
        {
            if (vyber == 1)
            {
                return Vyber.kamen;
            }
            else if (vyber == 2)
            {
                return Vyber.nuzky;
            }
            else
            {
                return Vyber.papir;
            }

        }

        static void Main(string[] args)
        {
            Vyber pocitac, hrac;
            int vyhraP = 0, vyhraH = 0, vyberP;
            string vyberH;

            while (true)
            {
                Console.Write("Vase volba (kamen, nuzky nebo papir): ");

                vyberH = Console.ReadLine();
                hrac = Dosad(vyberH);

                while (hrac == 0)
                {
                    Console.WriteLine("Spatny vstup, zkuste to znovu.");
                    Console.Write("Vase volba (kamen, nuzky nebo papir): ");
                    vyberH = Console.ReadLine();
                    hrac = Dosad(vyberH);
                }

                Random r = new Random();
                vyberP = r.Next(1, 4);
                pocitac = Dosad(vyberP);

                Console.WriteLine("Hrac vybral {0}, pocitac vybral {1}.", hrac, pocitac);

                if (hrac == pocitac)
                {
                    Console.WriteLine("Remiza, hrajeme dal.");
                    Console.WriteLine("Hrac {0}:{1} Pocitac", vyhraH, vyhraP);
                    continue;
                }
                else if ( (hrac == Vyber.kamen && pocitac == Vyber.nuzky) || (hrac == Vyber.nuzky && pocitac == Vyber.papir) || (hrac == Vyber.papir && pocitac == Vyber.kamen) )
                {
                    vyhraH++;
                    Console.WriteLine("Toto kolo vyhrava hrac.");
                }
                else if ((hrac == Vyber.kamen && pocitac == Vyber.papir) || (hrac == Vyber.nuzky && pocitac == Vyber.kamen) || (hrac == Vyber.papir && pocitac == Vyber.nuzky))
                {
                    vyhraP++;
                    Console.WriteLine("Toto kolo vyhrava pocitac.");
                }
                Console.WriteLine("Hrac {0}:{1} Pocitac", vyhraH, vyhraP);
                if (vyhraH == 2 || vyhraP == 2)
                    break;
            }

            if (vyhraH > vyhraP)
                Console.WriteLine("Vyhral hrac, gratuluji!");
            else
                Console.WriteLine("Vyhral pocitac, je mi to lito...");

            Console.ReadLine();
        }
    }
}
