﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni3._2
{
    class Program
    {
        static double ZadejVysku ()
        {
            Console.Write("Zadejte vysku: ");
            double x = Convert.ToDouble(Console.ReadLine());
            return x;
        }

        static double ZadejVahu()
        {
            Console.Write("Zadejte vahu: ");
            double y = Convert.ToDouble(Console.ReadLine());
            return y;
        }

        static void Vypis (double vysledek)
        {
            Console.Write("BMI: {0:N}", vysledek);
            Console.ReadLine();
        }

        static void Vypis(BodyMassIndex vysledek)
        {
            Console.WriteLine("Slovne: {0}", vysledek);
            Console.ReadLine();
        }

        enum BodyMassIndex
        {
            tezkaPodvyziva = 1,
            podvaha,
            optimalniVaha,
            nadvaha,
            obezitaPrvnihoStupne,
            obezitaDruhehoStupne,
            obezitaTretihoStupne
        }

        static double SpoctiBMI1 (double vy, double va)
        {
            double bmi = va / (vy * vy);
            return bmi;
        }

        static BodyMassIndex SpoctiBMI2 (double vy, double va) //odstran ify
        {
            double bmi = va / (vy * vy);

            if (bmi >= 0 && bmi < 16.5)
            {
                return BodyMassIndex.tezkaPodvyziva;
            }
            else if (bmi >= 16.5 && bmi < 18.5)
            {
                return BodyMassIndex.podvaha;
            }
            else if (bmi >= 18.5 && bmi < 25)
            {
                return BodyMassIndex.optimalniVaha;
            }
            else if (bmi >= 25 && bmi < 30)
            {
                return BodyMassIndex.nadvaha;
            }
            else if (bmi >= 30 && bmi < 35)
            {
                return BodyMassIndex.obezitaPrvnihoStupne;
            }
            else if (bmi >= 35 && bmi < 40)
            {
                return BodyMassIndex.obezitaDruhehoStupne;
            }
            else
            {
                return BodyMassIndex.obezitaTretihoStupne;
            }

        }

        static void Main(string[] args)
        {
            double vyska, vaha, BMI1;
            BodyMassIndex BMI2;

            vyska = ZadejVysku();
            vaha  = ZadejVahu ();
            BMI1 = SpoctiBMI1(vyska, vaha);
            BMI2 = SpoctiBMI2(vyska, vaha);

            Vypis(BMI1);
            Vypis(BMI2);
        }
    }
}
