﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni3._4
{
    class Program
    {
        struct KomplexniCislo
        {
            public double re;
            public double im;
        }

        static void Vypis(KomplexniCislo x)
        {
            if (x.im >= 0)
            {
                Console.Write("{0}+{1}i ", x.re, x.im);
                Console.ReadLine();
            }
            else
            {
                Console.Write("{0}{1}i ", x.re, x.im);
                Console.ReadLine();
            }
        }

        static KomplexniCislo Secti (KomplexniCislo a, KomplexniCislo b)
        {
            KomplexniCislo vysledek;

            vysledek.re = a.re + b.re;
            vysledek.im = a.im + b.im;

            return vysledek;
        }

        static KomplexniCislo Odecti (KomplexniCislo a, KomplexniCislo b)
        {
            KomplexniCislo vysledek;

            vysledek.re = a.re - b.re;
            vysledek.im = a.im - b.im;

            return vysledek;
        }

        static KomplexniCislo Vynasob (KomplexniCislo a, KomplexniCislo b)
        {
            KomplexniCislo vysledek;

            vysledek.re = a.re * b.re - a.im * b.im;
            vysledek.im = a.re * b.im + a.im * b.re;

            return vysledek;
        }

        static void Main(string[] args)
        {
            KomplexniCislo x, y, a, b, c;

            x.re = 5.2;
            x.im = 3.25;

            y.re = 1.6;
            y.im = -4;

            a = Secti(x, y);
            Vypis(a);

            b = Odecti(x, y);
            Vypis(b);

            c = Vynasob(x, y);
            Vypis(c);
        }
    }
}
