﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni3._7
{
    class Program
    {
        struct datum
        {
            public int den;
            public int mesic;
            public int rok;
        }

        static bool Prestupny(int rok)
        {
            bool prestup = false;

            if (rok % 4 == 0)
                prestup = true;
            if (rok % 100 == 0)
                prestup = false;
            if (rok % 400 == 0)
                prestup = true;

            return prestup;
        }

        static datum NasledneDatum(datum d, int dny)
        {
            for (int i=0; i < dny; i++)
            {
                if ( d.mesic == 4 || d.mesic == 6 || d.mesic == 9 || d.mesic == 11 )
                {
                    if (d.den == 30)
                    {
                        d.den = 1;
                        d.mesic++;
                        continue;
                    }
                    else
                    {
                        d.den++;
                        continue;
                    }
                }
                else if (d.mesic == 2)
                {
                    if (Prestupny(d.rok))
                    {
                        if (d.den == 29)
                        {
                            d.den = 1;
                            d.mesic++;
                            continue;
                        }
                        else
                        {
                            d.den++;
                            continue;
                        }

                    }
                    else
                    {
                        if (d.den == 28)
                        {
                            d.den = 1;
                            d.mesic++;
                            continue;
                        }
                        else
                        {
                            d.den++;
                            continue;
                        }
                    }
                }
                else
                {
                    if (d.mesic == 12 && d.den == 31)
                    {
                        d.rok++;
                        d.mesic = 1;
                        d.den = 1;
                        continue;
                    }
                    else if (d.mesic != 12 && d.den == 31)
                    {
                        d.den = 1;
                        d.mesic++;
                        continue;
                    }
                    else
                    {
                        d.den++;
                        continue;
                    }
                }
            }
            return d;
        }

        static void Main(string[] args)
        {
            datum d, d1;

            d.den = 16; d.mesic = 7; d.rok = 2015;
            d1 = NasledneDatum(d, 500);

            Console.Read();
        }
    }
}
