﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni3._3
{
    class Program
    {
        static void Vypis (double hodnota)
        {
            Console.Write("{0} ", hodnota);
            Console.ReadLine();
        }

        static double Faktorial (double cislo)
        {
            double puvodni = cislo;

            if (cislo == 0)
                return 1;

            for (int i = 1; i < puvodni; i++)
            {
                cislo *= i;
            }

            return cislo;
        }

        static double Mocnina (double cislo, double exponent)
        {
            double puvodni = cislo;

            if (exponent == 0)
                return 1;

            for (int i = 1; i < exponent; i++)
            {
                cislo *= puvodni;
            }

            return cislo;
        }

        static double Vyraz (double n)
        {
            double vysledek = 0;
            for (double k = 0; k <= 100; k++)
            {
                vysledek += Mocnina(n, k) / Faktorial(k);
            }
            return vysledek;
        }

        static double Kombinace (double n, double k)
        {
            double vysledek = 0;

            vysledek = Faktorial(n) / (Faktorial(k) * Faktorial(n - k));

            return vysledek;
        }

        static void Main(string[] args)
        {
            double x, y, z, a, b;

            x = Faktorial(11);
            Vypis(x);

            y = Mocnina(5, 3);
            Vypis(y);

            z = Mocnina(2.69, 7);
            Vypis(z);

            a = Vyraz(5.14);
            Vypis(a);

            b = Kombinace(49, 6); // 13983816 - spravne, 16983816 - spatne v materialech
            Vypis(b);
        }
    }
}
