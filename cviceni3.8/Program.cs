﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni3._8
{
    class Program
    {
        enum DenTydne
        {
            pondeli = 1,
            utery, 
            streda, 
            ctvrtek, 
            patek, 
            sobota, 
            nedele
        }

        struct datum
        {
            public int den;
            public int mesic;
            public int rok;
        }

        static bool Prestupny(int rok)
        {
            bool prestup = false;

            if (rok % 4 == 0)
                prestup = true;
            if (rok % 100 == 0)
                prestup = false;
            if (rok % 400 == 0)
                prestup = true;

            return prestup;
        }

        static DenTydne DenVTydnu(datum d)
        {
            datum start;
            DenTydne st;
            int dny = 0;
            start.den = 1; start.mesic = 1; start.rok = 1900; st = DenTydne.pondeli;

            while (start.rok < d.rok)
            {
                if ( Prestupny(start.rok) )
                {
                    st += 2;
                    if ( (int) st > 7)
                        st -= 7;
                }
                else
                {
                    st += 1;
                    if ( (int) st > 7)
                        st -= 7;
                }
                start.rok++;
            }

            while (start.mesic < d.mesic)
            {
                if (start.mesic == 2 && Prestupny(start.rok))
                {
                    st += 1;
                    if ((int)st > 7)
                        st -= 7;
                }
                else if (start.mesic == 2 && !Prestupny(start.rok))
                {
                    start.mesic++;
                    continue;
                }

                else if (start.mesic == 4 || start.mesic == 6 || start.mesic == 9 || start.mesic == 11)
                {
                    st += 2;
                    if ((int)st > 7)
                        st -= 7;
                }
                else
                {
                    st += 3;
                    if ((int)st > 7)
                        st -= 7;
                }
                start.mesic++;
            }

                st += (d.den - start.den) % 7;
                if ((int)st > 7)
                    st -= 7;

            return st;
        }

        static void Vypis (DenTydne den)
        {
            Console.Write("{0} ", den);
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            datum d;
            DenTydne den;
            d.den = 15; d.mesic = 3; d.rok = 1966;

            den = DenVTydnu(d);

            Vypis(den);
        }
    }
}