﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni3._1
{
    class Program
    {
        enum DPH
        {
            Zakladni     = 21,
            PrvniSnizena = 15,
            DruhaSnizena = 10
        }

        static double SpoctiDPH1 (double hodnota, int sazba)
        {
            return hodnota += (hodnota / 100) * sazba;
        }

        static double SpoctiDPH2(double hodnota, DPH sazba)
        {
            if (sazba == DPH.Zakladni)
                return hodnota += (hodnota / 100) * (int) DPH.Zakladni;
            if (sazba == DPH.PrvniSnizena)
                return hodnota += (hodnota / 100) * (int) DPH.PrvniSnizena;
            if (sazba == DPH.DruhaSnizena)
                return hodnota += (hodnota / 100) * (int) DPH.DruhaSnizena;

            return 0;
        }

        static void vypis (double promenna)
        {
            Console.WriteLine("Cena s DPH je: {0} ", promenna);
            Console.ReadLine();
        }

        static double nactiCenu ()
        {
            Console.Write("Zadejte cenu: ");
            double cena = Convert.ToDouble(Console.ReadLine());

            return cena;
        }

        static void Main(string[] args)
        {
            double zbozi = nactiCenu();
            double x = SpoctiDPH1(zbozi, 21);
            double y = SpoctiDPH2(zbozi, DPH.PrvniSnizena);

            vypis(x);
            vypis(y);
        }
    }
}
